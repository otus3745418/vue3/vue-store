import axios from 'axios';

const apiClient = axios.create({
    baseURL: 'https://fakestoreapi.com',
    withCredentials: false,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
    },
    timeout: 10000,
});

export const getProducts = async () => {
    try {
        const response = await apiClient.get('/products')
        return response.data
    } catch (error) {
        throw error
    }
}